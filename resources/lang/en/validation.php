<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'El :attribute debe ser aceptado.',
    'active_url'           => 'La :attribute no es una URL válida.',
    'after'                => 'La :attribute debe ser una fecha después de :date.',
    'after_or_equal'       => 'La :attribute debe ser una fecha después o igual a :date.',
    'alpha'                => 'El :attribute unicamente de contener letras.',
    'alpha_dash'           => 'El :attribute unicamente de contener letras, numeros, and dashes.',
    'alpha_num'            => 'El :attribute unicamente de contener letras and numeros.',
    'array'                => 'El :attribute debe ser un arreglo.',
    'before'               => 'El :attribute debe ser una fecha antes :date.',
    'before_or_equal'      => 'El :attribute debe ser una fecha antes o igual a :date.',
    'between'              => [
        'numeric' => 'El :attribute debe ser entre :min y :max.',
        'file'    => 'El :attribute debe ser entre :min y :max kilobytes.',
        'string'  => 'El :attribute debe ser entre :min y :max characters.',
        'array'   => 'El :attribute debe tener entre :min y :max items.',
    ],
    'boolean'              => 'El :attribute campo debe ser verdadero o falso.',
    'confirmed'            => 'El :attribute no ha marcado la confirmación.',
    'date'                 => 'El :attribute No es una fecha vália.',
    'date_format'          => 'El :attribute No ha seleccionado el formato :format.',
    'different'            => 'El :attribute y :other deben ser diferentes.',
    'digits'               => 'El :attribute debe ser de :digits dígitos.',
    'digits_between'       => 'El :attribute debe estar entre :min y :max dígitos.',
    'dimensions'           => 'El :attribute tiene una imagen con dimensiones no válidas.',
    'distinct'             => 'El :attribute campo tiene un valor ducplicado.',
    'email'                => 'El :attribute debe ser una dirección de correo válida.',
    'exists'               => 'El selected :attribute es inválido.',
    'file'                 => 'El :attribute debe ser un archivo.',
    'filled'               => 'El :attribute campo debe tener un valor.',
    'image'                => 'El :attribute debe ser una imagen.',
    'in'                   => 'El :attribute seleccionado es invlido.',
    'in_array'             => 'El :attribute campo no existe en :other.',
    'integer'              => 'El :attribute debe ser un entero.',
    'ip'                   => 'El :attribute debe ser una dirección IP válida.',
    'ipv4'                 => 'El :attribute debe ser una dirección IPv4 válida.',
    'ipv6'                 => 'El :attribute debe ser una dirección IPv6 válida.',
    'json'                 => 'El :attribute debe ser una dirección JSON cadena.',
    'max'                  => [
        'numeric' => 'El :attribute no puede ser mas grande que :max.',
        'file'    => 'El :attribute no puede ser mas grande que :max kilobytes.',
        'string'  => 'El :attribute no puede ser mas grande que :max caracteres.',
        'array'   => 'El :attribute no puede tener mas de :max items.',
    ],
    'mimes'                => 'El :attribute debe ser un archivo de tipo: :values.',
    'mimetypes'            => 'El :attribute debe ser un archivo de tipo: :values.',
    'min'                  => [
        'numeric' => 'El :attribute Debe ser menor a :min.',
        'file'    => 'El :attribute Debe ser menor a :min kilobytes.',
        'string'  => 'El :attribute Debe ser menor a :min characters.',
        'array'   => 'El :attribute Debe tener menos de :min items.',
    ],
    'not_in'               => 'El :attribute seleccionado es inválido.',
    'numeric'              => 'El :attribute debe ser un numero.',
    'present'              => 'El :attribute field debe ser present.',
    'regex'                => 'El :attribute format is invalid.',
    'required'             => 'El campo :attribute es requerido.',
    'required_if'          => 'El campo :attribute es requerido cuando :other is :value.',
    'required_unless'      => 'El campo :attribute es requerido a no ser que :other tenga un valor de :values.',
    'required_with'        => 'El campo :attribute es requerido cuando :values está presente.',
    'required_with_all'    => 'El campo :attribute es requerido cuando :values está presente.',
    'required_without'     => 'El campo :attribute es requerido cuando :values no está presente.',
    'required_without_all' => 'El campo :attribute es requerido cuando ninguno de :values están presentes.',
    'same'                 => 'El :attribute y :other deben se marcados.',
    'size'                 => [
        'numeric' => 'El :attribute debe ser :size.',
        'file'    => 'El :attribute debe ser :size kilobytes.',
        'string'  => 'El :attribute debe ser :size caracteres.',
        'array'   => 'El :attribute debe contener :size items.',
    ],
    'string'               => 'El :attribute debe ser una cadena de caracteres.',
    'timezone'             => 'El :attribute debe ser una zona válida.',
    'unique'               => 'El :attribute ya se ha tomado.',
    'uploaded'             => 'El :attribute falló la descarga.',
    'url'                  => 'El :attribute es un fromato inválido.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
