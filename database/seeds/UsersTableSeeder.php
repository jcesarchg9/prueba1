<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->insert([
            'email'      => 'jcesarchg9@gmail.com',
            'password'   => bcrypt('qwerty'),
            'username' => 'CESAR',
            'last_name'  => 'CHARACO',
        ]);

        \DB::table('users')->insert([
            'email'      => 'uptatareas@gmail.com',
            'password'   => bcrypt('qwerty'),
            'username' => 'PROFE',
            'last_name'  => 'CESAR',
        ]);

        \DB::table('role_users')->insert([
            'user_id' => 1,
            'role_id' => 1
        ]);

        \DB::table('leader')->insert([
            'id'   => 1,
            'user' => 1
        ]);

        \DB::table('role_users')->insert([
            'user_id' => 2,
            'role_id' => 2
        ]);

    }
}
