<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('roles')->insert(array (
            'slug'        => 'Admin',
            'name'        => 'Administrador',
            'permissions' => '{"configuracion.view" : true,"configuracion.edit" : true,"configuracion.delete" : true,"modulo1.view": true,"modulo1.create": true,"modulo1.edit" : true,"modulo1.delete" : false}'
        ));

        \DB::table('roles')->insert(array (
            'slug'        => 'Pers',
            'name'        => 'Persona',
            'permissions' => '{"modulo1.view": true,"modulo1.create": true,"modulo1.edit" : true}'
        ));
    }
}
