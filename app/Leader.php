<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leader extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'leader';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'user'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    public function users()
    {
    	return $this->belongsTo('App\User', 'user');
    }
}
